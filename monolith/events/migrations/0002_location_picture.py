# Generated by Django 5.0.6 on 2024-06-13 19:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("events", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="location",
            name="picture",
            field=models.URLField(null=True),
        ),
    ]
