import requests
from .keys import PEXELS_API_KEY
import json

def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    param ={
        "query": city + " " + state,
        "per_page": 1        
    }

#     r = requests.get("https://api.pexels.com/v1/search", params=param, headers=header)
#     content = json.loads(r.text)
#     if len(content["photos"]) < 1:
#         return {"picture_url": None}
#     results = {
#         "picture_url": content["photos"][0]["src"]["original"]
#     }
#     return results

# def get_weather_data(city, state):
#     url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
#     response = requests.get(url)
#     content = json.loads(response.text)

#     if not content or len(content) == 0:
#         return {"error": "No geocoding data found"}
    
#     lat = content[0]["lat"]
#     lon = content[0]["lon"]

#     url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=metric&appid={OPEN_WEATHER_API_KEY}" 
#     response = requests.get(url)
#     content = json.loads(response.text)

#     if 'main' not in content or 'weather' not in content:
#         return {"error": "Weather data not found"}
    
#     temp = content["main"]["temp"]
#     desc = content["weather"][0]["description"]

#     return {
#         "temperature": temp,
#         "description": desc
#     }
